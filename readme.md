# FBAuthLaravel
Laravel application that uses the Facebook PHP SDK v4

## Install
Clone the repository onto a web server that supports PHP 5.4+

Install the Laravel framework and teh Facebook API with composer

```$ composer install```

## Running the application
You need a Facebook API key and Secret key to use the Facebook API

Place those two keys into .env

You're set to go after all that!
