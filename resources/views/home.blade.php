@extends('layout.main')

@section('content')
	@if($fb->loggedIn())
		<?php $id = $fb->get('id'); ?>
		<p>Welcome {{ $fb->get('name') }}!</p>
		<p>Here are the latest status updates by you.</p>
		<?php $s = $fb->get('data', $id, 'feed', ['limit' => '10']); ?>
		<div class="panel panel-primary">
			<div class="panel-heading">Here is the output of the request</div>
			<div class="panel-body">
				<pre style="max-height: 250px; overflow: auto;">{{ var_dump($s) }}</pre>
			</div>
		</div>
		@if(!empty($s))
			@foreach($s->asArray() as $status)
				<div class="bs-callout bs-callout-primary">
					{!! nl2br($status->message) !!}
					<br/>
					<a href="https://facebook.com/{{ $status->id }}" target="_blank">View on Facebook</a>
					@if(isset($status->comments))
						<?php $c = $status->comments; ?>
						<hr/>
						{{ count($c->data) }} Comments
						@foreach($c->data as $comment)
							<div class="bs-callout bs-callout-danger" style="margin-left: 15px;">
								Comment by:
								<a href="https://facebook.com/{{ $comment->from->id }}" target="_blank">
									{{ $comment->from->name }}
								</a>
								<img src="{{ $fb->get(
								'url', $comment->from->id, 'picture',
								['redirect' => 'false', 'type' => 'large']
							) }}" style="width: 50px;"><br/>
								{{ $comment->message }}
							</div>
						@endforeach
					@endif
				</div>
			@endforeach
		@else
			<p>Apparently, there are no posts which can be retrieved from Facebook. Sorry! :(</p>
		@endif
	@else
		<p>Welcome to the Site! Using the site requires being logged in, however, it's done with.. you guessed it, Facebook! Use the login link at the top to login with Facebook.</p>
	@endif

@stop
