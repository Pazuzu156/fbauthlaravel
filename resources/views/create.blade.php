@extends('layout.main')

@section('content')

	@if($errors->has('message_post'))
		<div class="alert alert-danger alert-dismissable" role="alert">
			{{ $errors->first('message_post') }}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
	@endif
	<div class="well bs-component">
		{!! Form::open(['class' => 'form-horizontal']) !!}
			<fieldset>
				<legend>Creation Form</legend>
				<div class="form-group">
					{!! Form::textarea('message_post', '', ['placeholder' => 'Type your post here', 'class' => 'form-control']) !!}
				</div>
				{!! Form::submit('Post', ['class' => 'btn btn-primary']) !!}
				<a href="{{ URL::action('HomeController@index') }}" class="btn btn-default">Cancel</a>
			</fieldset>
		{!! Form::close() !!}
	</div>

@stop
