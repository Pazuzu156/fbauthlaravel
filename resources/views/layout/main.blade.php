<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Facebook Website</title>
	<meta name="viewport" content="width=device-width, user-scalable=no">
	{!! Html::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css') !!}
	{!! Html::style('css/darkly.css') !!}
	{!! Html::script('//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js') !!}
	{!! Html::script('//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js') !!}
	{!! Html::style('css/default.css') !!}
</head>
<body>
	<nav class="navbar navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<span class="navbar-brand">Facebook Website</span>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Mobile Menu</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse navbar-right bs-navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="{{ URL::action('HomeController@index') }}">Home</a></li>
					@if($fb->loggedIn())
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="{{ $fb->get('url', $fb->get('id'), 'picture', ['redirect' => 'false', 'type' => 'large']) }}" style="width: 20px;">
							{{ $fb->get('name') }}
						</a>
						<ul class="dropdown-menu">
							<li><a href="{{ URL::route('get-create') }}">Post to Facebook</a></li>
							<li><a href="{{ URL::action('HomeController@logout') }}">Logout</a></li>
						</ul>
					</li>
					@else
					<li><a href="{{ $fb->getLoginUrl(['email', 'user_posts', 'publish_actions']) }}">Login</a></li>
					@endif
				</ul>
			</div>
		</div>
	</nav>
	<div class="container">
		<h2>{{ $pagename }}</h2>
		<div class="col-sm-8">
			@if(Session::has('global'))
				<div class="alert alert-success alert-dismissable" role="alert">
					{{ Session::get('global') }}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@endif
			@yield('content')
		</div>
		@if($fb->loggedIn())
			<div class="col-sm-4">
				<div class="panel panel-warning">
					<div class="panel-heading">Your Access Token</div>
					<div class="panel-body" style="overflow: auto;">{{ Session::get('facebook') }}</div>
				</div>
				<div class="panel panel-warning">
					<div class="panel-heading">Your Facebook ID</div>
					<div class="panel-body">{{ $fb->get('id') }}</div>
				</div>
			</div>
		@endif
	</div>
</body>
</html>