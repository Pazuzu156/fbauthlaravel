<?php namespace App\Internal\KalebKlein;

use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequestException;
use Facebook\FacebookSession;
use Facebook\FacebookRequest;

use Session;

/**
 * Class Facebook
 * Do I need to explain? Implementation of the Facebook API
 * @package App\Internal\KalebKlein
 */
class Facebook
{
	private $_helper, $_session;

	public function __construct($appId, $appSecret)
	{
		FacebookSession::setDefaultApplication($appId, $appSecret);
	}

	/**
	 * Registers a new login helper using the provided redirect URL
	 * @param $redirectUrl
	 * @return $this
	 */
	public function registerLoginHelper($redirectUrl)
	{
		$this->_helper = new RedirectLoginHelper($redirectUrl);
		return $this;
	}

	/**
	 * Returns the generated login URL
	 * @param array $scope - Scope is any extra permissions required by the application
	 * @return mixed
	 */
	public function getLoginUrl($scope=array())
	{
		if(empty($scope))
			return $this->_helper->getLoginUrl();
		else
			return $this->_helper->getLoginUrl($scope);
	}

	/**
	 * Returns the helper class
	 * @return mixed
	 */
	public function getHelper()
	{
		return $this->_helper;
	}

	/**
	 * Returns the FB Session
	 * @return mixed
	 */
	public function getSession()
	{
		return $this->_session;
	}

	/**
	 * Creates a new FB session using the FB Helper
	 * @param FacebookRedirectLoginHelper $helper
	 * @return Facebook
	 * @throws \Exception
	 */
	public function setSession(FacebookRedirectLoginHelper $helper)
	{
		try
		{
			$this->_session = $helper->getSessionFromRedirect();
		}
		catch(FacebookRequestException $ex)
		{
			throw new \Exception("Failed to get session from Facebook. Error: " . $ex->getMessage());
		}
		catch(\Exception $ex)
		{
			throw new \Exception("Internal exception. Error: " . $ex->getMessage());
		}

		return $this;
	}

	/**
	 * Creates a new session from the API auth token
	 * @param $token
	 */
	private function setSessionFromToken($token)
	{
		$this->_session = new FacebookSession($token);
	}

	/**
	 * Creates a Laravel session using the user's Auth token
	 */
	public function registerUserInfo()
	{
		Session::put('facebook', $this->_session->getToken());
	}

	/**
	 * Checks if a user is logged in or not
	 * @return bool
	 */
	public function loggedIn()
	{
		return(Session::has('facebook')) ? true : false;
	}

	/**
	 * Gets something from FB. Things like name, email, statuses, etc.
	 * @param $get - The piece of data you need
	 * @param string $id - The authenticated user's ID
	 * @param string $item - The item you're looking for
	 * @param array $params - Parameters to fine-tune the search
	 * @return mixed
	 * @throws FacebookRequestException
	 */
	public function get($get, $id = '', $item = '', $params = array())
	{
		// This tid-bit here parses the params and
		// pastes them all together
		$end = '';
		if(!empty($params))
		{
			$end = '?';
			foreach($params as $key => $value)
				$end .= "$key=$value&";
		}

		$headers = (empty($id)) ? '/me' : "/$id/$item/" . rtrim($end, '&'); // The scope of the request
		$this->setSessionFromToken(Session::get('facebook'));
		$graph = (new FacebookRequest(
			$this->getSession(), 'GET', $headers
		))->execute()->getGraphObject();

		if($get!='')
			return $graph->getProperty($get);
		else
			return $graph;
	}

	/**
	 * Sends data to FB. FB Permissions ARE required to make these kinds of requests!
	 * @param $id - The authenticated user's ID
	 * @param array $params - Parameters required to fine tune the request
	 * @return mixed
	 * @throws FacebookRequestException
	 * @throws \Exception
	 */
	public function post($id, $params=array())
	{
		if(empty($params))
			throw new \Exception("You must provide an array for the params parameter!");

		$this->setSessionFromToken(Session::get('facebook'));
		$graph = (new FacebookRequest(
			$this->getSession(), 'POST', "/$id/feed",
			$params
		))->execute()->getGraphObject();

		return $graph;
	}

	/**
	 * Logs out the user
	 */
	public function logout()
	{
		if($this->loggedIn())
			Session::forget('facebook');

		Session::flash('global', 'You have been logged out!');
	}

	/**
	 * Returns the user's Access token. Same a using Session::get('facebook');
	 * @return mixed
	 */
	public function getAccessToken()
	{
		return Session::get('facebook');
	}
}
