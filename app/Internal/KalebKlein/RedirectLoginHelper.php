<?php namespace App\Internal\KalebKlein;

use Facebook\FacebookRedirectLoginHelper as Helper;
use Session;

/**
 * Class RedirectLoginHelper
 * Overrides the default FacebookRedirectLoginHelper class
 * to allow for Laravel's sessions rather than the default PHP ones
 * @package App\Internal\KalebKlein
 */
class RedirectLoginHelper extends Helper
{
	protected function storeState($state)
	{
		Session::put('state', $state);
	}

	protected function loadState()
	{
		return $this->state = Session::get('state');
	}
}