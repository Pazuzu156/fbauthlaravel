<?php namespace App\Http\Middleware;

use Closure;
use Session;

class FacebookAuth
{
	/**
	 * Makes sure the user is logged in before processing requests
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if(!Session::has('facebook'))
			return redirect()->to('/');

		return $next($request);
	}

}
