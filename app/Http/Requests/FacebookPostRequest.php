<?php namespace App\Http\Requests;

use Session;

/**
 * Class FacebookPostRequest
 * Handles POST request validation for posting to Facebook
 * @package App\Http\Requests
 */
class FacebookPostRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		if(Session::has('facebook'))
			return true;

		return false;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'message_post' => 'required'
		];
	}

}
