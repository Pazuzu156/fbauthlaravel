<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\FacebookPostRequest;

use Session;
use Input;

class HomeController extends Controller
{
	private $_helper;

	/**
	 * Controllers constructor. Uses the parent controller's FB object
	 * to create a local helper with the helper registration method
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_helper = $this->fb->registerLoginHelper(route('login'));
	}

	/**
	 * Gets the index page of the site
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		$headers = [
			'pagename' => 'Home',
			'fb' => $this->_helper
		];
		return view('home', $headers);
	}

	/**
	 * Processes the login functionality after being redirected back from Facebook
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function login()
	{
		// Check if FB is returning with an error or not
		if(Input::has('error'))
		{
			// Self explanitory, user denied authorization
			if(Input::get('error_reason') == 'user_denied')
				Session::flash('global', 'You have aborted the Facebook authorization!');
		}
		else
		{
			try
			{
				$this->fb->setSession($this->fb->getHelper()); // Create FB session with it's helper
			}
			catch(\Exception $ex)
			{
				echo $ex->getMessage();
				exit;
			}

			// Check if user successfully connected with FB
			if($this->fb->getSession())
			{
				$this->fb->registerUserInfo(); // Login the user!
				Session::flash('global', 'You have successfully logged in!');
			}
		}

		return redirect()->action('HomeController@index');
	}

	/**
	 * GET request that returns the post create page
	 * @return \Illuminate\View\View
	 */
	public function getCreate()
	{
		$headers = [
			'pagename' => 'Post to Facebook',
			'fb' => $this->_helper
		];
		return view('create', $headers);
	}

	/**
	 * POST request that handles posting to FB
	 * @param FacebookPostRequest $request
	 * @return $this|\Illuminate\Http\RedirectResponse
	 * @throws \Exception
	 */
	public function postCreate(FacebookPostRequest $request)
	{
		$fb = $this->_helper;
		$post = $fb->post($fb->get('id'), [
			'message' => $request->get('message_post')
		]);

		if($post)
		{
			Session::flash('global', 'The post was successfully sent to Facebook!');
			return redirect()->action('HomeController@index');
		}
		else
		{
			Session::flash('global', 'An error occurred when posting to Facebook!');
			return redirect()->route('get-create')->withInput();
		}
	}

	/**
	 * Yeah..ya know, logout!
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function logout()
	{
		$this->fb->logout();
		return redirect()->action('HomeController@index');
	}
}
