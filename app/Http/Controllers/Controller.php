<?php namespace App\Http\Controllers;

use App\Internal\KalebKlein\Facebook;
use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;

	protected $fb;

	public function __construct()
	{
		$this->fb = new Facebook(env('FB_APP_ID'), env('FB_APP_SECRET'));
	}

}
