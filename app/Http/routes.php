<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

get('/', 'HomeController@index');
get('/login', [
	'as' => 'login',
	'uses' => 'HomeController@login'
]);

Route::group(['middleware' => 'auth'], function()
{
	get('/post/{id}', [
		'as' => 'post',
		'uses' => 'HomeController@getPost'
	]);
	get('/logout', 'HomeController@logout');
	get('/create', [
		'as' => 'get-create',
		'uses' => 'HomeController@getCreate'
	]);
	post('/create', 'HomeController@postCreate');
});
